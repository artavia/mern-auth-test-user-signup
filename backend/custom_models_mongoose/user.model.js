const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const SALT_ROUNDS = 10;
const bcrypt = require("bcrypt");


let UserSchema = new Schema( {
  // username: { type: String }
  username: { type: String, required: true, unique: true }
  // , email: { type: String }
  , email: { type: String, required: true, unique: true }
  // , password: { type: String }
  , password: { type: String, required: true }
} );

UserSchema.pre( "save" , function(next){
  
  // only has the pdub if new or modified
  if( !this.isModified('password') ){ 
    return next(); 
  }
  if (this.isNew || this.isModified('password')){
    
    let document = this;
    
    // asyncronously generate a salt
    bcrypt.genSalt( SALT_ROUNDS , function( err, salt ){
      
      if( err ){ 
        next(err); // return next(err); 
      } 

      // asyncronously hash the pdub using the new salt
      bcrypt.hash( document.password, salt, function( err, hashedPassword ){
        
        if( err ){ 
          next(err); // return next(err); 
        }

        document.password = hashedPassword;
        next();
      } );

    } );
  }
  else{
    next();
  }

} );

UserSchema.methods.comparePdub = function(plntxtpdub, cb ){
  bcrypt.compare( plntxtpdub , this.password, function( err, isMatch ){
    if( err ){ return cb(err); } 
    // cb( err, isMatch ); // generate the generic status text error from here
    cb( null, isMatch );   // generate the specific error text from the route
  } );
};

module.exports = mongoose.model( "User", UserSchema );

