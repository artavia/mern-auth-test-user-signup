/* =============================================
Authentication
  - authentication -- process of verifying user identity 
    - (401 Unauthorized status code/text)
  - authorization -- process of verifying user permissions 
    - (403 Forbidden status code/text)

username/password scheme
  - stateful  (e.g. - session using a cookie)
  - stateless (e.g. - token using JWT / OAuth / other...)
============================================= */

const confirmToken = (req, res, next ) => {
  
  // const header = req.headers['x-access-token']; // v1
  const header = req.headers['authorization']; // v2
  
  if( typeof header !== 'undefined') {
    // const token = header; // v1
    const token = header.split(' ')[1]; // v2

    req.token = token;
    next();
  } 
  else {
    res.status(403).json( { errormessage: 'Unauthorized request: No token provided.' } );
  }
  
};

module.exports = confirmToken;