db.users.drop();
db.events.drop();
db.specialevents.drop();

db.users.insert( 
	{
		username: 'clarkkent' ,
		email: "clark@dailyplanet.com"
		// , password: "password" 
		, password: "$2b$10$6IcvWx9hKsjvtMfiennSc.RLYTWrFFDjyC1DHj5E0LkGGnNf77Rru"
	} 
);

db.events.insert( {
	name : "Alpha Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Beta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Charlie Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Delta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Elephant Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Foxtrot Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Alpha Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Beta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Charlie Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Delta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Elephant Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Foxtrot Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );