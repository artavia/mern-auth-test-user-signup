// new boilerplate
// =============================================
const cors = require("cors");
// const bodyParser = require("body-parser");
// const helmet = require("helmet"); // https://expressjs.com/en/advanced/best-practice-security.html

// BASE SETUP
// =============================================
const express = require("express");
const app = express();

// =============================================
// process.env SETUP
// =============================================
const { 
  PORT
  , SERVERHOSTNAME
} = process.env;

// IMPORT DB MIDDLEWARE
// =============================================
const connectDB = require('./custom_db/database-obj');

// ROUTER SETUP
// =============================================
const apiRouter = require("./routes/api");

// APP SETUP
// =============================================
app.use( cors() );
// See the react auth blog in which cors is required for access
/* app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization');
  next();
}); */

// app.use( bodyParser.json() );
app.use( express.json() ); // for parsing application/json
app.use( express.urlencoded( { extended: true } ) ); // for parsing application/x-www-form-urlencoded
// app.use( express.urlencoded( { extended: false } ) ); // no reason for this to be on...

// ----------------------------------------------------------------------
// | helmet                                                              |
// ----------------------------------------------------------------------
// app.use( helmet() ); 
// app.use( helmet.noCache() ); // https://helmetjs.github.io/docs/nocache/ 


// ----------------------------------------------------------------------
// | trust proxy setting                                                |
// | this permits the assignment of secure cookies which is choking
// | at the moment because of express-session
// ----------------------------------------------------------------------
app.enable( 'trust proxy' , 1 );

// ----------------------------------------------------------------------
// | Server-side technology information                                 |
// | Remove the `X-Powered-By` response header that
// | contributes to header bloat and can unnecessarily expose vulnerabilities
// ----------------------------------------------------------------------
app.disable( 'x-powered-by' );

// ----------------------------------------------------------------------
// | ETags                                                              |
// | PERFORMANCE helper function
// | Remove `ETags` as resources are sent with far-future expires headers
// | https://developer.yahoo.com/performance/rules.html#etags
// ----------------------------------------------------------------------
app.set( 'etag' , false );

app.use( '/api' , apiRouter ); 

// SERVER INSTANTIATION
// =============================================
/* app.listen( PORT , () => { 
  // console.log( "process.env.CONNECTION_URL" , process.env.CONNECTION_URL );
  console.log( `Server is running on port ${PORT}` ); 
} ); */

/* app.listen( PORT, SERVERHOSTNAME, null, () => { 
  console.log( `Backend running at http://${SERVERHOSTNAME}:${PORT} . ` ); 
} ); // PORT, hostname, backlog, cb  */

// DB and BACKEND SERVERS INSTANTIATION
// =============================================
connectDB().then( async () => {
  await app.listen( PORT, SERVERHOSTNAME, null, () => { 
    console.log( `Backend running at http://${SERVERHOSTNAME}:${PORT} . ` ); 
  } ); // PORT, hostname, backlog, cb  
} );