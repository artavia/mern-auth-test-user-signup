import React from 'react';

import { AuthService } from '../services/AuthService';

class RegisterPage extends React.Component{
  
  constructor(){
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this);
    
    this.state = { username: '', email: '', password: '', error: false  };
    this.Auth = new AuthService();
  }

  onChangeInput( event ){
    this.setState( {
      [ event.target.id ] : event.target.value
    } );
  }

  async handleSubmit( event ){
    
    event.preventDefault();

    // console.log( `Form submitted - ${ this.state.username }` );
    // console.log( `Form submitted - ${ this.state.email }` );
    // console.log( `Form submitted - ${ this.state.password }` );

    const alluserspromise = await this.Auth.getAllUsers()
    .then( data => { // console.log( "data" , data );
      return data;
    } )
    .catch( (err) => { // console.log( "err", err );
      this.setState( { error: err.message } );
    } );

    // console.log( "alluserspromise" , alluserspromise );

    if( alluserspromise !== undefined ){

      this.setState( { error: false } );

      let value;
      const doesUsernameExist = await alluserspromise.some( user => user.username === this.state.username );
      const doesEmailExist = await alluserspromise.some( user => user.email === this.state.email );
      if( doesUsernameExist || doesEmailExist ){
        value = await true;
      }
      if( !doesUsernameExist && !doesEmailExist ){
        value = await false;
      }

      if( value === true ){
        this.setState( { error: "Username or email exist. Try again!" } );
      }

      if( value === false ){
        
        this.Auth.registerNewUser( { username: this.state.username , email: this.state.email, password: this.state.password } )
        .then( (data) => { // console.log( "data" , data );
          if( !!data.errormessage ){
            this.setState( { error: data.errormessage } );
          }
          else
          if( !data.errormessage ){
            this.props.history.push('/login');
            // this.props.history.replace('/login');
          }
        } )
        .catch( (err) => { // console.log( "err.message", err.message );
          this.setState( { error: err.message } );
        } );

      }

    }

  }

  render(){ 

    let errormessage = (
      <>
        <h2>An error has occurred</h2>
        <p>{ this.state.error }</p>
      </>
    );
    
    let element = (
      <>
        <div className="py-5">
          <div className="row">
            <div className="col-md-6 mx-auto">
              <span className="anchor" id="formRegister"></span>

              <div className="card rounded-0">
                <div className="card-header">
                  <h3 className="mb-0">Register</h3>
                </div>

                <div className="card-body">
                  
                  <form className="form" onSubmit={ this.handleSubmit } >

                    <div className="form-group">
                      <label htmlFor="username">Username</label>
                      <input type="text" name="username" id="username" className="form-control rounded-0" placeholder="johndoe" required value={ this.state.username } onChange={ this.onChangeInput } />
                    </div>
                    
                    <div className="form-group">
                      <label htmlFor="email">Email</label>
                      <input type="text" name="email" id="email" className="form-control rounded-0" placeholder="john@doe.com" required value={ this.state.email } onChange={ this.onChangeInput } />
                    </div>

                    <div className="form-group">
                      <label htmlFor="password">Password</label>
                      <input type="password" name="password" id="password" className="form-control rounded-0" placeholder="password" required value={ this.state.password } onChange={ this.onChangeInput } />
                    </div>

                    <button type="submit" disabled={ this.state.username === '' || this.state.email === '' || this.state.password === '' } className="btn btn-primary float-right">Register</button>

                  </form>

                </div>

              </div>

            </div>
          </div>
        </div>

        { !!this.state.error && errormessage }
      </>
    );
    return element;
  }
}

export {RegisterPage};