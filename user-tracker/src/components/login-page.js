import React from 'react';

import { AuthService } from '../services/AuthService';

class LoginPage extends React.Component{
  
  constructor(){
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this);

    this.state = { email: '', password: '', error: false };
    this.Auth = new AuthService();
  }

  componentDidMount(){ // UNSAFE_componentWillMount(){ 
    if( this.Auth.loggedIn() ){
      this.props.history.replace('/special');
      // this.props.history.replace('/test-dashboard');
    } 
  }

  onChangeInput( event ){
    this.setState( {
      [ event.target.id ] : event.target.value
    } );
  }

  handleSubmit( event ){ 
    event.preventDefault();

    // console.log(`Form submitted - ${ this.state.email }`);
    // console.log(`Form submitted - ${ this.state.password }`);
    
    this.setState( { error: false } );

    this.Auth.login( { email: this.state.email , password: this.state.password } )
    .then( (data) => {
      // console.log( "data" , data );
      if( !!data.errormessage ){
        this.setState( { error: data.errormessage } );
      }
      else
      if( !data.errormessage ){
        this.props.history.replace('/special');
        // this.props.history.replace('/test-dashboard');
      }
    } )
    .catch( (err) => {
      // console.log( "err.message", err.message );
      this.setState( { error: err.message } );
    } );
    
  }

  render(){ 
    // console.log( "this.props", this.props );

    let errormessage = (
      <>
        <h2>An error has occurred</h2>
        <p>{ this.state.error }</p>
      </>
    );
    
    let element = (
      <>
        <div className="py-5">
          <div className="row">
            <div className="col-md-6 mx-auto">
              <span className="anchor" id="formLogin"></span>

              <div className="card rounded-0">
                <div className="card-header">
                  <h3 className="mb-0">Login</h3>
                </div>

                <div className="card-body">
                  
                  <form className="form" onSubmit={ this.handleSubmit } >
                    
                    <div className="form-group">
                      <label htmlFor="email">Email</label>
                      <input type="text" name="email" id="email" className="form-control rounded-0" placeholder="john@doe.com" required value={ this.state.email } onChange={ this.onChangeInput } />
                    </div>

                    <div className="form-group">
                      <label htmlFor="password">Password</label>
                      <input type="password" name="password" id="password" className="form-control rounded-0" placeholder="password" required value={ this.state.password } onChange={ this.onChangeInput } />
                    </div>

                    <button type="submit" disabled={ this.state.email === '' || this.state.password === '' } className="btn btn-success float-right">Login</button>

                  </form>

                </div>

              </div>

            </div>
          </div>
        </div>

        { !!this.state.error && errormessage }

      </>
    );
    return element;
  }
}

export {LoginPage};