import React from 'react';
import { NavLink } from 'react-router-dom';

class DisplayCard extends React.Component{
  
  constructor(){
    super();
    this.noOpLink = this.noOpLink.bind(this);
  }

  noOpLink(event){
    event.preventDefault(); 
  }

  render(){ 
    
    // console.log( "this.props", this.props );

    let element = (
      <>
        <div className="col-md-4 mb-3">
          <div className="card text-center">
            <div className="card-body">
              <h5 className="card-title"> {this.props.event.name} </h5>
              <p className="card-text"> {this.props.event.description} </p>
              <NavLink to="#" className={ this.props.customtype === "specialevent" ? "btn btn-danger" : "btn btn-primary" } onClick={ this.noOpLink }>Buy Tickets</NavLink>
            </div>
            <div className="card-footer text-muted">
              {this.props.event.date} 
            </div>
          </div>
        </div>
      </>
    );
    return element;
  }
}

export {DisplayCard};