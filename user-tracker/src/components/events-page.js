import React from 'react';

import { DisplayCard } from './display-card';

class EventsPage extends React.Component{
  
  constructor(){
    super();
    this.state = { events: [] , error: false };
  }

  componentDidMount(){
    this.loadIt();
  }

  loadIt(){
    let baseUrl = "http://localhost:4000/api";
    
    fetch( `${baseUrl}/events` )
    .then( response => { // console.log( "response" , response );
      return response.json();
    } )
    .then( data => { // console.log( "data" , data );
      this.setState( { events: data } );
    } )
    .catch( (err) => { // console.log( "err.message", err.message );
      this.setState( { error: err.message } );
    } );
  }

  mapEvents(el,idx,arr){
    return <DisplayCard key={idx} event={el} customtype={'event'} />
  }

  eventList(){
    return this.state.events.map( this.mapEvents );
  }

  render(){ 

    let errormessage = (
      <>
        <h2>An error has occurred</h2>
        <p>{ this.state.error }</p>
      </>
    );
    
    let element = (
      <>
        
        <h1 className="display-3">Public Events</h1>
        <div className="row mt-5">
          { this.eventList() }
        </div>
        
        { !!this.state.error && errormessage }

      </>
    );
    return element;
  }
}

export {EventsPage};