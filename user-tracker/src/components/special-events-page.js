import React from 'react';

import { DisplayCard } from './display-card';

import { withAuth } from '../hoc/withAuth';

class SpecialEventsPage extends React.Component{
  
  constructor(){
    super();
    this.state = { specialevents: [] , error: false };
  }

  componentDidMount(){
    this.loadIt();
  }

  loadIt(){
    let baseUrl = "http://localhost:4000/api";
    
    fetch( `${baseUrl}/special` )
    .then( response => { // console.log( "response" , response );
      return response.json();
    } )
    .then( data => { // console.log( "data" , data );
      this.setState( { specialevents: data } );
    } )
    .catch( (err) => { // console.log( "err.message", err.message );
      this.setState( { error: err.message } );
    } );

  }

  mapEvents( el, idx, arr ){
    return <DisplayCard key={idx} event={el} customtype={'specialevent'} />
  }

  eventList(){
    return this.state.specialevents.map( this.mapEvents );
  }

  render(){ 

    let errormessage = (
      <>
        <h2>An error has occurred</h2>
        <p>{ this.state.error }</p>
      </>
    );
    
    let element = (
      <>

        <h1 className="display-3">Members&#45;Only Events</h1>
        
        <h2 className="display-4">
          Welcome, { this.props.profile.user.username }!!!
        </h2>
        
        <div className="row mt-5">
          { this.eventList() }
        </div>

        { !!this.state.error && errormessage }

      </>
    );
    return element;
  }
}

// export { SpecialEventsPage };
const HamperBerriesSnapperPants = withAuth( SpecialEventsPage );
export { HamperBerriesSnapperPants };