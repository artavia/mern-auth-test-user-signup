import React , { useState } from 'react';
import { useEffect , useCallback } from 'react';

import { useHistory } from 'react-router-dom';
import { AuthService } from "../services/AuthService";

const withAuth = ( AuthComponent ) => {
  
  const Auth = new AuthService();
  
  return function AuthWrapped(){
    
    // state = { profile: null };
    const [ profile, setProfile ] = useState( null );
    
    let history = useHistory(); // console.log( "history" , history );

    // V1
    /*
    useEffect( () => {

      let init = () => {
        if( !Auth.loggedIn() ){
          history.replace('/login');
        }
        else
        if( Auth.loggedIn() ){
          try{
            const userprofile = Auth.getProfile(); // console.log( "profile" , profile );
            setProfile( userprofile );
          }
          catch(error){ // console.log( "error", error );
            Auth.logout();
            history.replace('/login');
          }
        }
      };

      init();

      return () => {}; 

    // } );                        // effect runs everytime
    // } , [] );                   // effect runs on mount and unmount 
    } , [ history ] );          // effect runs when variable(s) change
    */

    // V2
    let innerFunction = useCallback( () => {
      if( !Auth.loggedIn() ){
        history.replace('/login');
      }
      else
      if( Auth.loggedIn() ){
        try{
          const userprofile = Auth.getProfile(); // console.log( "profile" , profile );
          setProfile( userprofile );
        }
        catch(error){ // console.log( "error", error );
          Auth.logout();
          history.replace('/login');
        }
      }
    } , [ history ] );

    useEffect( () => {

      innerFunction();
      return () => {};

    } , [ innerFunction ] );
    

    let element = (
      <AuthComponent history={ history } profile={ profile } />
    );

    if( profile ){
      return element;
    }
    else{
      return null;
    }

  };

};

export { withAuth };