import React, { useState, useEffect } from 'react';

import { DisplayCard } from './display-card';
import { withAuth } from '../functional-hoc/withAuth';

const SpecialEventsPage = ( props ) => {
  
  // this.state = { specialevents: [] , error: false };
  const [ specialevents, setSpecialEvents ] = useState( [] );
  const [ error, setError ] = useState(false);

  // componentDidMount(){ this.loadIt(); }
  useEffect( () => {
    
    const loadIt = () => {
      let baseUrl = "http://localhost:4000/api";
      
      fetch( `${baseUrl}/special` )
      .then( response => { // console.log( "response" , response );
        return response.json();
      } )
      .then( data => { // console.log( "data" , data );
        setSpecialEvents( data );
      } )
      .catch( (err) => { // console.log( "err.message", err.message );
        setError( err.message );
      } );
    };

    loadIt();

  } , [] );

  const mapEvents = (el,idx,arr) => {
    return <DisplayCard key={idx} event={el} customtype={'specialevent'} />
  }

  const eventList = () => {
    return specialevents.map( mapEvents );
  }
  
  let errormessage = (
    <>
      <h2>An error has occurred</h2>
      <p>{ error }</p>
    </>
  );
  
  let element = (
    <>
      <h1 className="display-3">Members&#45;Only Events</h1>
      <h2 className="display-4">
        Welcome, { props.profile.user.username }!!!
      </h2>
      <div className="row mt-5">
        { eventList() }
      </div>
      
      { !!error && errormessage }

    </>
  );
  return element;
};

const HamperBerriesSnapperPants = withAuth( SpecialEventsPage );
export { HamperBerriesSnapperPants };