import React from 'react';
import { NavLink } from 'react-router-dom';

const DisplayCard = ( props ) => {

  // console.log( "props", props );

  const noOpLink = (event) => {
    event.preventDefault(); 
  };
  
  let element = (
    <>
      <div className="col-md-4 mb-3">
        <div className="card text-center">
          <div className="card-body">
            <h5 className="card-title"> {props.event.name} </h5>
            <p className="card-text"> {props.event.description} </p>
            <NavLink to="#" className={ props.customtype === "specialevent" ? "btn btn-danger" : "btn btn-primary" } onClick={ noOpLink }>Buy Tickets</NavLink>
          </div>
          <div className="card-footer text-muted">
            {props.event.date} 
          </div>
        </div>
      </div>
    </>
  );

  return element;
};

export { DisplayCard };