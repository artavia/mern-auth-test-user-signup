import React, { useState, useEffect , useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthService } from '../services/AuthService';

const LoginPage = () => {
  
  const Auth = new AuthService(); // this.Auth = new AuthService();
  const history = useHistory(); // console.log( "history" , history );
  
  const [ logged, setLogged ] = useState( Auth.loggedIn() );

  // state = { email: '', password: '', error: false  };
  const [ email, setEmail ] = useState('');
  const [ password, setPassword ] = useState(''); 
  const [ error, setError ] = useState(false);  

  // useEffect - v1

  /* useEffect( () => {
    const isLoggedIn = () => {
      if( logged === true ){
        setLogged(true);
        history.replace('/special');
      }
    };
    isLoggedIn();
    return () => { };
  } , [ logged, history] ); */
  
  // } );                        // effect runs everytime and is OK
  // } , [] );                   // effect runs on mount and unmount  -- with warning...
  // } , [ logged, history ] );     // effect runs when variable(s) change  -- most efficient
  

  // useEffect - v2
  const innerFunction = useCallback( () => {
    if( logged === true ){
      setLogged(true);
      history.replace('/special');
    }
  } , [ logged, history ] );

  useEffect( () => {
    innerFunction();
    return () => {};
  } , [ innerFunction ] );

  const onChangeEmail = ( event ) => { setEmail( event.target.value ); };
  const onChangePassword = ( event ) => { setPassword( event.target.value ); };

  const handleSubmit = (event) => {
    event.preventDefault();

    // console.log(`Form submitted - ${ email }`);
    // console.log(`Form submitted - ${ password }`);

    setError( false );

    Auth.login( { email: email , password: password } )
    .then( (data) => { // console.log( "data" , data );
      if( !!data.errormessage ){
        setError( data.errormessage );
      }
      else
      if( !data.errormessage ){
        history.replace('/special');
        // history.replace('/test-dashboard');
      }
    } )
    .catch( (err) => { // console.log( "err.message", err.message );
      setError( err.message );
    } );

  };

  let errormessage = (
    <>
      <h2>An error has occurred</h2>
      <p>{ error }</p>
    </>
  );
  
  let element = (
    <>
      <div className="py-5">
        <div className="row">
          <div className="col-md-6 mx-auto">
            <span className="anchor" id="formLogin"></span>

            <div className="card rounded-0">
              <div className="card-header">
                <h3 className="mb-0">Login</h3>
              </div>

              <div className="card-body">
                
                <form className="form" onSubmit={ handleSubmit } >
                  
                  <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input type="text" name="email" id="email" className="form-control rounded-0" placeholder="john@doe.com" required value={ email } onChange={ onChangeEmail } />
                  </div>

                  <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input type="password" name="password" id="password" className="form-control rounded-0" placeholder="password" required value={ password } onChange={ onChangePassword } />
                  </div>

                  <button type="submit" disabled={ email === '' || password === '' } className="btn btn-success float-right">Login</button>

                </form>

              </div>

            </div>

          </div>
        </div>
      </div>

      { !!error && errormessage }

    </>
  );
  return element;
};

export { LoginPage };