import React, { useState, useEffect } from 'react';

import { DisplayCard } from './display-card';

const EventsPage = () => {

  // this.state = { events: [] , error: false };

  const [ events, setEvents ] = useState( [] );
  const [ error, setError ] = useState(false);

  // componentDidMount(){ this.loadIt(); }
  useEffect( () => {
    
    const loadIt = () => {
      let baseUrl = "http://localhost:4000/api";
      
      fetch( `${baseUrl}/events` )
      .then( response => { // console.log( "response" , response );
        return response.json();
      } )
      .then( data => { // console.log( "data" , data );
        setEvents( data );
      } )
      .catch( (err) => { // console.log( "err.message", err.message );
        setError( err.message );
      } );
    };

    loadIt();

  } , [] );

  const mapEvents = (el,idx,arr) => {
    return <DisplayCard key={idx} event={el} customtype={'event'} />
  }

  const eventList = () => {
    return events.map( mapEvents );
  }
  
  let errormessage = (
    <>
      <h2>An error has occurred</h2>
      <p>{ error }</p>
    </>
  );
  
  let element = (
    <>
      
      <h1 className="display-3">Public Events</h1>
      <div className="row mt-5">
        { eventList() }
      </div>
      
      { !!error && errormessage }

    </>
  );
  return element;
};

export { EventsPage };