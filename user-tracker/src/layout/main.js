import React from 'react';

import { Nav } from "./nav";

const Main = ( props ) => {
  
  // console.log( 'Main' , props );
  // console.log( 'Main' , props.children );  
  
  let element = (
    <>
      <Nav />
      <main role="main" className="container">
        <div className="starter-template">
          { props.children }
        </div>
      </main>
    </>
  );

  return element;
};

export {Main};