import React from 'react';
import { NavLink , useHistory } from 'react-router-dom';

import { AuthService } from '../services/AuthService';

const Nav = () => {
  
  const Auth = new AuthService();
  const history = useHistory(); // console.log( "history" , history );
  
  const noOpLink = (event) => { event.preventDefault(); };
  const handleLogout = (event) => {
    
    event.preventDefault();

    Auth.logout();
    history.replace('/login');
  }

  // console.log( "Auth.loggedIn()" , Auth.loggedIn() );

  let element = (

    <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      
      <NavLink className="navbar-brand" to="/" onClick={ noOpLink }>Test Auth App</NavLink>
      
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarsExampleDefault">
        
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <NavLink className="nav-link" to={"/events"} activeClassName={"active"} exact>
              Events
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to={"/special"} activeClassName={"active"} exact>
              Members
            </NavLink>
          </li>
        </ul>

        <ul className="navbar-nav">

          { !Auth.loggedIn() && <> <li className="nav-item">
            <NavLink className="nav-link btn btn-outline-success my-2 my-sm-0 mr-sm-2" to={"/login"} activeClassName={"active"} exact>
              Login
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link btn btn-outline-primary my-2 my-sm-0 mr-sm-2" to={"/register"} activeClassName={"active"} exact>
              Register
            </NavLink>
          </li> </> }

          { Auth.loggedIn() && <> <li className="nav-item">
            <NavLink className="nav-link btn btn-outline-danger my-2 my-sm-0 mr-sm-2" to={""} activeClassName={"active"} exact onClick={ handleLogout }>
              Logout
            </NavLink>
          </li> </> }

        </ul>

      </div>

    </nav>
  );
  return element;
};

export {Nav};