import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';


import { EventsPage } from '../functional-components/events-page';
import { RegisterPage } from '../functional-components/register-page';
import { LoginPage } from '../functional-components/login-page';
import { HamperBerriesSnapperPants } from '../functional-components/special-events-page';


// import { EventsPage } from '../components/events-page';
// import { RegisterPage } from '../components/register-page';
// import { LoginPage } from '../components/login-page';
// import { HamperBerriesSnapperPants } from '../components/special-events-page';

import { Main } from './main';

const Alpha = () => {
  
  let element = ( 
    <BrowserRouter>
      <Main>
        <Switch>
          <Route path="/events" exact component={EventsPage} />
          <Route path="/login" component={LoginPage} />
          <Route path="/register" component={RegisterPage} />
          <Route path="/special" component={HamperBerriesSnapperPants} />
          <Redirect from="/" to="/events" />
        </Switch>
      </Main>
    </BrowserRouter>
  );
  
  return element;

};

export {Alpha};