import React from 'react';
import { AuthService } from "../services/AuthService";

const withAuth = ( AuthComponent ) => {
  
  const Auth = new AuthService();

  return class AuthWrapped extends React.Component{
    
    constructor(){
      super();
      this.state = { profile: null };
    }

    componentDidMount(){ // UNSAFE_componentWillMount(){

      if( !Auth.loggedIn() ){
        this.props.history.replace('/login');
      }
      else{
        try{
          const profile = Auth.getProfile(); // console.log( "profile" , profile );
          this.setState( {
            profile: profile
          } );
        }
        catch(error){ // console.log( "error", error );
          Auth.logout();
          this.props.history.replace('/login');
        }
      }
      
    };

    render(){
      let element = (
        <AuthComponent history={this.props.history} profile={this.state.profile} />
      );
      if( this.state.profile ){
        return element;
      }
      else{
        return null;
      }
    };

  };
  
};

export { withAuth };