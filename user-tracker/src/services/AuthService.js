import decode from 'jwt-decode';

class AuthService{
  
  constructor(){ 
    this.baseUrl = 'http://localhost:4000/api'; 
    this.tokenKey = 'id_token';
    this.customfetch = this.customfetch.bind(this);
    this.login = this.login.bind(this);
    this.getProfile = this.getProfile.bind(this);
  }

  customfetch( url, options ){
    
    let myRequest = new Request( url );

    let myHeaders = new Headers();
    myHeaders.append( 'Content-type' , 'application/json; charset=UTF-8' ); 
    myHeaders.append( "Accept" , "application/json" );
    if( this.loggedIn() ){
      myHeaders.append( "Authorization" , `Bearer ${this.getToken() }` );
    }

    let optionsobject = { headers: myHeaders, ...options };

    return fetch( myRequest, optionsobject )
    .then( this.checkStatus )
    .then( response => response.json() );
  }

  checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return Promise.resolve(response); // return response;
    } 
    else {
      let error = new Error( response.statusText );
      error.response = response;
      return Promise.reject( error ); // throw error;
    }
  }

  login( userobj ){
    let myRequest = new Request( `${this.baseUrl}/login` );
    let optionsobject = { method : 'POST', body : JSON.stringify(userobj) };

    return this.customfetch( myRequest, optionsobject )
    .then( (data) => { // console.log( "data" , data );
      this.setToken( data.token );
      return Promise.resolve( data );
    } );
  }

  getAllUsers(){
    let usersRequest = new Request( `${this.baseUrl}/users` );
    let usersOptions = {};

    return this.customfetch( usersRequest, usersOptions )
    .then( (data) => { // console.log( "getAllUsers... data" , data );
      return Promise.resolve( data );
    } );

  }

  registerNewUser( newuserobj ){
    
    let registerRequest = new Request( `${this.baseUrl}/register` );
    
    let registerOptions = {
      method: 'POST'
      , body: JSON.stringify(newuserobj)
    };

    return this.customfetch( registerRequest, registerOptions )
    .then( ( data ) => { // console.log( "registerNewUser... data" , data );
      return Promise.resolve( data );
    } );

  }

  getProfile(){ // console.log( "decode( this.getToken() )" , decode( this.getToken() ) );
    return decode( this.getToken() );
  }

  loggedIn(){
    const token = this.getToken();
    return !!token && !this.isTokenExpired( token );
  }

  isTokenExpired(token){
    try{
      const decoded = decode( token ); // console.log( "decoded" , decoded );
      if( decoded.exp < Date.now() / 1000 ){
        return true;
      }
      else{
        return false;
      }
    }
    catch(err){ // console.log( "err", err );
      return false;
    }
  }

  setToken(token){
    localStorage.setItem( this.tokenKey , token );
  }

  getToken(){
    return localStorage.getItem( this.tokenKey );
  }

  logout(){
    localStorage.removeItem( this.tokenKey )
  }

};

export { AuthService };